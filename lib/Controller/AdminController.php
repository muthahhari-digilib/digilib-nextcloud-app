<?php
namespace OCA\DigitalLibraryApp\Controller;

use OCP\AppFramework\Controller;
use OCP\IGroupManager;
use OCP\IRequest;
use OCP\IUserSession;

class AdminController extends Controller {
	const NS_PREFIX = "{http://owncloud.org/ns}";

	/** @var IGroupManager|\OC\Group\Manager */ // FIXME Requires a method that is not on the interface
	protected $groupManager;

	/** @var IUserSession */
	private $userSession;

	public function __construct(
			$appName,
			IRequest $request,
			IGroupManager $groupManager,
			IUserSession $userSession
	){
		parent::__construct($appName, $request);
		
		$this->groupManager = $groupManager;
		$this->userSession = $userSession;
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @return JSONResponse
	 */
	public function isDigilibAdmin() {
		$user = $this->userSession->getUser();

		$subAdminManager = $this->groupManager->getSubAdmin();
		return array(
			'isDigilibAdmin'=> $subAdminManager->isSubAdmin($user)
		);
	}
}
