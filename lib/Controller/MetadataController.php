<?php
namespace OCA\DigitalLibraryApp\Controller;

use Exception;
use OCA\DigitalLibraryApp\Db\DigitalLibraryMetadata;
use OCA\DigitalLibraryApp\Service\MetadataService;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\StreamResponse;
use OCP\Files\IRootFolder;
use OCP\ILogger;
use OCP\IRequest;
use OCP\IUserSession;
use Psr\Log\LoggerInterface;

function endsWith($haystack, $needle) {
    return substr_compare($haystack, $needle, -strlen($needle)) === 0;
}

class MetadataController extends Controller {
	const NS_PREFIX = "{http://owncloud.org/ns}";

	/**
	 * @var MetadataService
	 */
	private $metadataService;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var IRootFolder
	 */
	private $storage;

	/**
	 * @var IUserSession
	 */
	private $userSession;

	/*
	 * @var IRequest
	 */
	protected $request;

	public function __construct(
			$appName,
			IRequest $request,
			MetadataService $metadataService,
			IUserSession $userSession,
			LoggerInterface $logger,
			IRootFolder $storage
	){
		parent::__construct($appName, $request);
		$this->metadataService = $metadataService;
		$this->userSession = $userSession;
		$this->logger = $logger;
		$this->storage = $storage;
		$this->request = $request;
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @return JSONResponse
	 */
	public function index(int $docId): JSONResponse {
		$m = $this->metadataService->getMetadata("/doc/$docId");
		return new JSONResponse($m);
	}


	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @return JSONResponse
	 */
	public function update(int $docId, array $metadata): JSONResponse {
		$m = $metadata;
		unset($m['publishPath']);
		
		$m = $this->metadataService->saveMetadata($this->userSession->getUser(), "/doc/$docId", $m);
		if (!isset($metadata['state'])) { 
			if ($m->getState() == DigitalLibraryMetadata::STATE_PUBLISHED) {
				$this->metadataService->saveMetadata($this->userSession->getUser(), "/doc/$docId", ['state' => DigitalLibraryMetadata::STATE_PENDING_SYNC_METADATA]);
				$metadata['state'] = DigitalLibraryMetadata::STATE_PENDING_SYNC_METADATA;
			} else if ($m->getState() == DigitalLibraryMetadata::STATE_PENDING_SYNC_CONTENT) {
				$this->metadataService->saveMetadata($this->userSession->getUser(), "/doc/$docId", ['state' => DigitalLibraryMetadata::STATE_PENDING_PUBLISH]);
				$metadata['state'] = DigitalLibraryMetadata::STATE_PENDING_PUBLISH;
			}
		}
		
		// pakai yang full
		if (isset($metadata['state'])) {
			$metadata = $this->metadataService->addSync($docId, "/doc/$docId", $metadata);
		}

		return new JSONResponse($metadata);
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 */
	public function count(array $data): JSONResponse {
		$res = [];
		foreach ($data as $el) {
			$path = endsWith($el['path'], "/") ? $el['path'] : "{$el['path']}/";
			$res[] = $path  . $el['name'];
		}
		$ret = [];
		return new JSONResponse($this->metadataService->getMetadataCount($this->userSession->getUser()->getUID(), $res));
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @return JSONResponse
	 */
	public function options(): JSONResponse {
		$res = $this->metadataService->getOptions();
		return new JSONResponse($res);
	}

	/**
	 * @SubAdminRequired
	 * @NoCSRFRequired
	 */
	public function addTag(string $tag, string $type) {
		$res = $this->metadataService->addTag($type, $tag);
		return new JSONResponse([
			"success" => true
		]);
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @return StreamResponse
	 */
	public function artwork(string $artworkFile): StreamResponse {
		$result = preg_replace("/[^a-zA-Z0-9\\.\\+]+/", "", $artworkFile);

		return new StreamResponse('/opt/digilibaw/' . $result, );
	}

	/**
	 * @SubAdminRequired
	 * @NoCSRFRequired
	 * @return JSONResponse
	 */
	public function uploadArtwork(): JSONResponse {
		$file = $this->request->getUploadedFile("artworkFile");

		$imageFileType = strtolower(pathinfo($_FILES["artworkFile"]["name"],PATHINFO_EXTENSION));
		$target_file = md5_file($_FILES["artworkFile"]["tmp_name"]) . '.' . $imageFileType;

		move_uploaded_file($_FILES["artworkFile"]["tmp_name"], '/opt/digilibaw/' . $target_file);
		
		return new JSONResponse(["hash" => $target_file]);
	}
	
}
