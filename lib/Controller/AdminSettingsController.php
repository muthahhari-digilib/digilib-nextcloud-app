<?php

declare(strict_types=1);

namespace OCA\DigitalLibraryApp\Controller;

use OCA\DigitalLibraryApp\Service\MetadataService;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IRequest;

class AdminSettingsController extends Controller {
    function __construct(
        $appName,
        IRequest $request,
        MetadataService $metadataService
    ) {
        parent::__construct($appName, $request);

        $this->metadataService = $metadataService;
    }

    /**
     * @NoCSRFRequired
     */
    public function index() {
        return new TemplateResponse('digitallibraryapp', 'admin');
    }

    /**
     * @NoCSRFRequired
     */
    public function options(): JSONResponse {
        $res = $this->metadataService->getOptions();
        return new JSONResponse($res);
    }

    /**
     * @NoCSRFRequired
     */
    public function addTag(string $type, string $tag) {
        if (!$this->metadataService->isExists($type, $tag)) {
            $this->metadataService->addTag($type, $tag);
        }
    }

    /**
     * @NoCSRFRequired
     */
    public function removeTag(string $type, string $tag) {
        $this->metadataService->removeTag($type, $tag);
    }
}

?>