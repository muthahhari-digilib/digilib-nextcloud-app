<?php

namespace OCA\DigitalLibraryApp\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\QBMapper;
use OCP\IDBConnection;

class TagMapper extends QBMapper {
    /**
     * @param IDBConnection $db
     */
    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'digital_library_tag', DigitalLibraryTag::class);
    }

    public function getAllTag() {
        $qb = $this->db->getQueryBuilder();

        $q = $qb->select('*')
            ->from($this->tableName);
        
        return $this->findEntities($q);
    }

    private function find(string $type, string $tag) {
        $qb = $this->db->getQueryBuilder();

        $q = $qb->select('*')
            ->from($this->tableName, 't')
            ->where('t.type = :type AND t.tag = :tag')
            ->setParameter('type', $type)
            ->setParameter('tag', $tag)
            ->setMaxResults(1);
        
        return $this->findEntity($q);
    }

    public function isExists(string $type, string $tag) {
        try {
            $this->find($type, $tag);
            return true;
        } catch(DoesNotExistException $err) {
            return false;
        }
    }

    public function addTag(string $type, string $tag) {
        $t = new DigitalLibraryTag();

        $t->setType($type);
        $t->setTag($tag);

        $this->insert($t);
    }

    public function removeTag(string $type, string $tag) {
        $column = '';
        if ($type == 'people') { $column = 'people'; }
        else if ($type == 'locations') { $column = 'locations'; }
        else if ($type == 'events') { $column = 'events'; }
        else if ($type == 'years') { $column = 'years'; }
        else if ($type == 'months') { $column = 'months'; }
        else if ($type == 'topics') { $column = 'topics'; }

        if ($column == '') {
            throw new DoesNotExistException($type);
        }

        $tag = DigitalLibraryTag::escapeTag($tag);
        $tag = str_replace("(", "\\(", $tag);
        $tag = str_replace(")", "\\)", $tag);
        $pattern1 = "^{$tag};|;{$tag};";
        $pattern2 = "^{$tag}$|;{$tag}$";

        $this->db->executeStatement(
            "UPDATE *PREFIX*digital_library_metadata " .
            "   SET \"{$column}\" = regexp_replace( " .
            "   regexp_replace(people, :pattern1, ';'), :pattern2, '')"
        , [
            "pattern1" => $pattern1,
            "pattern2" => $pattern2
        ]);
        $t = $this->find($type, $tag);
        $this->delete($t);
    }
}


?>