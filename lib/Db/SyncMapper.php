<?php

namespace OCA\DigitalLibraryApp\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\QBMapper;
use OCP\IDBConnection;
use ReflectionClass;

class SyncMapper extends QBMapper {
    /**
     * @param IDBConnection $db
     */
    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'digital_library_sync', DigitalLibrarySync::class);
    }

    public function addOrUpdateSync(string $path, string $digilibPath, string $filePath) {
        $qb = $this->db->getQueryBuilder();

        $q = $qb->select('*')
                ->from($this->tableName, 't')
                ->where('t.path = :path')
                ->setParameter('path', $path)
                ->setMaxResults(1);
        try {
            $e = $this->findEntity($q);

            $e->setDigilibPath($digilibPath);
            $e->setFilePath($filePath);

            $e = $this->update($e);

            return $e;
        } catch(DoesNotExistException $err) {
            $e = new DigitalLibrarySync();

            $e->setPath($path);
            
            $e->setDigilibPath($digilibPath);
            $e->setFilePath($filePath);

            $e = $this->insert($e);

            return $e;
        }
    }


}


?>