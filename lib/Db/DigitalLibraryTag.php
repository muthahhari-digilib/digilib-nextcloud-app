<?php

namespace OCA\DigitalLibraryApp\Db;

use OCP\AppFramework\Db\Entity;

class DigitalLibraryTag extends Entity {
    public $type;
    public $tag;

    public function __construct() {
        $this->addType('type', 'string');
        $this->addType('tag', 'string');
    }

    public static function escapeTag(string $tag) {
        // kebutuhan ini: aman untuk digunakan di regular expression
        // perkecualian di sini adalah ( dan ) yang memang dibutuhkan
        // dan tidak ada ';'
        // saat ini yang diperbolehkan di tag hanya sesuai yang ada di sini
        return preg_replace("/[^a-z '-]/i", '', $tag);
    }
}


?>