<?php

namespace OCA\DigitalLibraryApp\Db;

use OCP\AppFramework\Db\Entity;

class DigitalLibraryMetadata extends Entity {
    public $path;
    public $type;
    public $topic;
    public $authors;

    /* delimited dengan ; */
    public $people;
    /* delimited dengan ; */
    public $locations;
    /* delimited dengan ; */
    public $events;

    public $years;
    /* delimited dengan ; */
    public $months;
    public $topics;

    public $sourceUrl;
    public $imageHash;

    public $originalPublisher;
    public $copyrightHolder;

    public $numberOfCopy;
    /**
     * Unpublished
     * Pending Publish
     * Pending Sync Metadata
     * Published
     * Pending Sync Content
     */
    public $state;

    const STATE_PENDING_UNPUBLISH = 'Pending Unpublish';
    const STATE_UNPUBLISHED = 'Unpublished';
    const STATE_PENDING_PUBLISH = 'Pending Publish';
    const STATE_PUBLISHING = 'Publishing';
    const STATE_PENDING_SYNC_METADATA = 'Pending Sync Metadata';
    const STATE_SYNCING_METADATA = 'Syncing Metadata';
    const STATE_PENDING_SYNC_CONTENT = 'Pending Sync Content';
    const STATE_PUBLISHED = 'Published';

    public $description;

    public $createUser;
    public $createDate;
    public $writeUser;
    public $writeDate;
    public $publishUser;
    public $publishDate;

    public function __construct() {
        $this->addType('topic', 'string');
        $this->addType('type', 'string');
        $this->addType('path', 'string');
        $this->addType('authors', 'string');
        $this->addType('people', 'string');
        $this->addType('locations', 'string');
        $this->addType('events', 'string');
        $this->addType('years', 'string');
        $this->addType('months', 'string');
        $this->addType('topics', 'string');
        $this->addType('sourceUrl', 'string');
        $this->addType('imageHash', 'string');
        $this->addType('originalPublisher', 'string');
        $this->addType('copyrightHolder', 'string');
        $this->addType('numberOfCopy', 'int');
        $this->addType('state', 'string');
        $this->addType('description', 'string');

        $this->addType('createUser', 'string');
        $this->addType('createDate', 'string');
        $this->addType('writeUser', 'string');
        $this->addType('writeDate', 'string');
        $this->addType('publishUser', 'string');
        $this->addType('publishDate', 'string');
    }
}


?>