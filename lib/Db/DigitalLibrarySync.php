<?php

namespace OCA\DigitalLibraryApp\Db;

use OCP\AppFramework\Db\Entity;

class DigitalLibrarySync extends Entity {
    public $path;
    public $digilibPath;
    public $filePath;

    public function __construct() {
        $this->addType('path','string');
        $this->addType('digilibPath', 'string');
        $this->addType('filePath','string');
    }
}


?>