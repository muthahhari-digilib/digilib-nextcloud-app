<?php

namespace OCA\DigitalLibraryApp\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\QBMapper;
use OCP\IDBConnection;
use OCP\IUser;
use ReflectionClass;

const EXPLODES = ['authors', 'people', 'locations', 'events', 'years', 'months', 'topics'];

class MetadataMapper extends QBMapper {
    /**
     * @param IDBConnection $db
     */
    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'digital_library_metadata', DigitalLibraryMetadata::class);
    }

    /**
     * throws DoesNotExistException if not exists
     */
    private function getByPath(string $path): DigitalLibraryMetadata {
        $qb = $this->db->getQueryBuilder();

        $q = $qb->select('*')
                  ->from($this->tableName, 't')
                 ->where('t.path = :path')
                 ->setParameter('path', $path)
                 ->setMaxResults(1);

        return $this->findEntity($q);
    }

    public function getMetadataByPath(string $path): array {
        try {
            $m = $this->getByPath($path);
            $ret = [];
            $refclass = new ReflectionClass($m);
            $ret["path"] = $path;
            foreach ($refclass->getProperties() as $property) {
                $value = $m->{$property->name};
                if (in_array($property->name, EXPLODES)) {
                    if ($value == null) {
                        $value = [];
                    } else {
                        $value = explode(';', $value);
                    }
                }
                $ret[$property->name] = $value;
            }
            return $ret;
        } catch(DoesNotExistException $err) {
            return ["path" => $path];
        }
    }

    public function isExists(string $path): bool {
        $qb = $this->db->getQueryBuilder();

        $q = $qb->select('id')
                  ->from($this->tableName, 't')
                 ->where('t.path = :path')
                 ->setParameter('path', $path)
                 ->setMaxResults(1);

        try {
            $this->findOneQuery($q);
            return true;
        } catch(DoesNotExistException $err) {
            return false;
        }
    }

    public function create(string $path, IUser $user, array $metadata) {
        $m = new DigitalLibraryMetadata();

        $m->setPath($path);

        foreach ($metadata as $key => $value) {
            if (in_array($key, EXPLODES)) {
                $tags = [];
                foreach ($value as $tag) {
                    $tags[] = DigitalLibraryTag::escapeTag($tag);
                }
                $value = implode(';', $tags);
            }
            $m->{'set' . ucfirst($key)}($value);
        }

        $m->setCreateUser($user->getUID());
        $m->setCreateDate(gmstrftime("%Y-%m-%d %H:%M:%S"));
        $m = $this->insert($m);

        return $m;
    }

    public function save(string $path, IUser $user, array $metadata) {
        $m = $this->getByPath($path);
        foreach ($metadata as $key => $value) {
            if (in_array($key, EXPLODES)) {
                $value = implode(';', $value);
            }
            $m->{'set' . ucfirst($key)}($value);
        }

        $t = gmstrftime("%Y-%m-%d %H:%M:%S");
        if ($m->state == DigitalLibraryMetadata::STATE_PENDING_PUBLISH) {
            $m->setPublishUser($user->getUID());
            $m->setPublishDate($t);
        } else {
            $m->setWriteUser($user->getUID());
            $m->setWriteDate($t);
        }
        $m = $this->update($m);

        return $m;
    }

    public function countMetadata(array $idsMap) {
        $values = [];
        $params = [];
        $i = 1;
        foreach ($idsMap as $key => $value) {
            $values[] = "(:path{$i}, :ids{$i}::text[])" ;
            $params["path{$i}"] = $key;
            $params["ids{$i}"] = "{" . implode(",", array_map(function($a) { return "/doc/{$a}"; }, $value)) . "}";
            $i++;
        }
        $valuesQuery = implode(",", $values);
        $res = $this->db->executeQuery(<<<EOF
            SELECT p,
                   (select count(*) from *PREFIX*digital_library_metadata m
                     where ((authors is not null and authors != '')
                            or (people is not null and people != '')
                            or (locations is not null and locations != '')
                            or (events is not null and events != '')
                            or (years is not null and years != '')
                            or (months is not null and months != '')
                            or (topics is not null and topics != ''))
                       and m.path = any(ids)
                   )
              FROM (VALUES{$valuesQuery}) AS a(p, ids)
            EOF, $params
        );
        return $res->fetchAll();
    }
}


?>