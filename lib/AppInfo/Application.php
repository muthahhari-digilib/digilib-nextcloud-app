<?php

namespace OCA\DigitalLibraryApp\AppInfo;

use OCA\DigitalLibraryApp\Listener\LoadSidebarListener;
use OCA\Files\Event\LoadSidebar;
use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\AppFramework\QueryException;
use OCP\Util;

class Application extends App implements IBootstrap {
    const APP_NAME = 'digitallibraryapp';

    /**
     * Application constructor.
     *
     * @param array $params
     * @throws QueryException
     */
    public function __construct(array $params = [])
    {
        parent::__construct(self::APP_NAME, $params);
    }
    
    public function register(IRegistrationContext $context): void {
        // Load scripts for sidebar.
        $context->registerEventListener(
            LoadSidebar::class,
            LoadSidebarListener::class
        );
    }

    public function boot(IBootContext $context): void {
    }
}
