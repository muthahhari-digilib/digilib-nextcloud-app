<?php

namespace OCA\DigitalLibraryApp\Service;

use OCA\DigitalLibraryApp\Db\DigitalLibraryMetadata;
use OCA\DigitalLibraryApp\Db\DigitalLibraryTag;
use OCA\DigitalLibraryApp\Db\MetadataMapper;
use OCA\DigitalLibraryApp\Db\SyncMapper;
use OCA\DigitalLibraryApp\Db\TagMapper;
use OCP\Files\FileInfo;
use OCP\Files\Folder;
use OCP\Files\IRootFolder;
use OCP\Files\Node;
use OCP\Files\NotFoundException;
use OCP\IUser;

class MetadataService {
    const NS_PREFIX = "{http://owncloud.org/ns}";

    /**
     * @var TagMapper
     */
    private $tagMapper;

    /**
     * @var MetadataMapper
     */
    private $metadataMapper;

    /**
     * @var SyncMapper
     */
    private $syncMapper;

    /**
     * @var IRootFolder
     */
    private $root;

    public function __construct(
        TagMapper $tagMapper, MetadataMapper $metadataMapper, SyncMapper $syncMapper,
        IRootFolder $root
    ) {
        $this->tagMapper = $tagMapper;
        $this->metadataMapper = $metadataMapper;
        $this->syncMapper = $syncMapper;
        $this->root = $root;
    }

    public function getOptions(): array {
        $ret = [
            "authors" => [],
            "people" => [],
            "locations" => [],
            "events" => [],
            "years" => [],
            "months" => [],
            "topics" => [],
        ];
        $res = $this->tagMapper->getAllTag();
        foreach ($res as $value) {
            error_log($value->type . "/" . $value->tag);
            $ret[$value->type][] = $value->tag;
        }
        return $ret;
    }

    public function addTag(string $type, string $tag) {
        if (!$this->tagMapper->isExists($type, $tag)) {
            $this->tagMapper->addTag($type, $tag);
        }
    }

    public function getMetadata(string $path) {
        return $this->metadataMapper->getMetadataByPath($path);
    }

    public function saveMetadata(IUser $user, string $path, array $metadata) {
        if ($this->metadataMapper->isExists($path)) {
            return $this->metadataMapper->save($path, $user, $metadata);
        } else {
            $metadata['state'] = DigitalLibraryMetadata::STATE_UNPUBLISHED;
            return $this->metadataMapper->create($path, $user, $metadata);
        }
    }

    public function addSync(int $fileId, string $path, array $metadata) {
        $digilibPath = $metadata['publishPath'];

        $files = $this->root->getById($fileId);
        $filePath = $files[0]->getMountPoint()->getMountPoint() . '$$$$$/////$$$$$' .
		$files[0]->getInternalPath();
	if ($files[0]->getMountPoint()->getMountType() == 'group') {
        	$filePath = $files[0]->getMountPoint()->getSourcePath() . '$$$$$/////$$$$$' .
			$files[0]->getInternalPath();
	}

        if ($digilibPath == null) {
            $digilibPath = $files[0]->getPath();
        }
        
        $this->syncMapper->addOrUpdateSync($path, $digilibPath, $filePath);
        return [
            'path' => $path,
            'digilibPath' => $digilibPath,
            'filePath' => $filePath
        ];
    }

    private function getFileIds(Folder $folder, string $path) {
        try {
            $node = $folder->get($path);
            return $this->getFileIdsRec($node);
        } catch (NotFoundException $ex) {
            return [];
        }
    }
    private function getFileIdsRec(Node $node) {
        if ($node->getType() == FileInfo::TYPE_FILE) {
            return [$node->getId()];
        } else {
            $ret = [];
            foreach ($node->getDirectoryListing() as $child) {
                foreach ($this->getFileIdsRec($child) as $id) {
                    $ret[] = $id;
                }
            }
            return $ret;
        }
    }
    public function getMetadataCount(string $userId, array $fullPaths) {
        $folder = $this->root->getUserFolder($userId);
        $ids = [];
        foreach ($fullPaths as $path) {
            $ids[$path] = $this->getFileIds($folder, $path);
        }
        $ret = [];
        $res = $this->metadataMapper->countMetadata($ids);
        foreach($res as $row) {
            $ret[$row['p']] = "{$row['count']} / " . count($ids[$row['p']]);
        }
        return $ret;
    }
}
?>
