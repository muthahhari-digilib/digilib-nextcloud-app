<?php

declare(strict_types=1);

namespace OCA\DigitalLibraryApp\Listener;

use OCA\DigitalLibraryApp\AppInfo\Application;
use OCA\Files\Event\LoadSidebar;
use OCP\EventDispatcher\IEventListener;
use OCP\Util;
use Symfony\Contracts\EventDispatcher\Event;

class LoadSidebarListener implements IEventListener {
    public function handle(Event $event): void {
        if (!($event instanceof LoadSidebar)) {
            return;
        }

        Util::addScript(Application::APP_NAME, 'digitallibraryapp');
    }
}


?>
