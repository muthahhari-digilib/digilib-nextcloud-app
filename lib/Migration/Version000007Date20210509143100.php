<?php

use OCP\Migration\IOutput;
use OCP\Migration\SimpleMigrationStep;

class Version000007Date20210509143100 extends SimpleMigrationStep {
    /**
    * @param IOutput $output
    * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
    * @param array $options
    * @return null|ISchemaWrapper
    */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();
        
        $table = $schema->getTable('digital_library_metadata');
        if (!$table->hasColumn('description')) {
            $table->addColumn('description', 'string', ['notnull' => false, 'length' => 4000]);
        }

        return $schema;
    }
}


?>