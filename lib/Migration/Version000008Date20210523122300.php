<?php

use OCP\Migration\IOutput;
use OCP\Migration\SimpleMigrationStep;

class Version000007Date20210509143100 extends SimpleMigrationStep {
    /**
    * @param IOutput $output
    * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
    * @param array $options
    * @return null|ISchemaWrapper
    */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();
        
        if (!$schema->hasTable('digital_library_sync')) {
            $table = $schema->createTable('digital_library_sync');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true,
            ]);
            $table->addColumn('path', 'string', ['notnull' => true, 'length' => 2000]);
            $table->addColumn('digilib_path', 'string', ['notnull' => true, 'length' => 4000]);
            $table->addColumn('file_path', 'string', ['notnull' => true, 'length' => 4000]);

            $table->setPrimaryKey(['id']);
        }

        return $schema;
    }
}

?>