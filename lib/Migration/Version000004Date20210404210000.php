<?php

namespace OCA\DigitalLibraryApp\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;


class Version000004Date20210404210000 extends SimpleMigrationStep {
    /**
    * @param IOutput $output
    * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
    * @param array $options
    * @return null|ISchemaWrapper
    */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();

        if (!$schema->hasTable('digital_library_tag')) {
            $table = $schema->createTable('digital_library_tag');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true,
            ]);
            $table->addColumn('type', 'string', [
                'notnull' => true,
                'length' => 10
            ]);
            $table->addColumn('tag', 'string', [
                'notnull' => true,
                'length' => 200,
            ]);

            $table->setPrimaryKey(['id']);
        }

        if (!$schema->hasTable('digital_library_metadata')) {
            $table = $schema->createTable('digital_library_metadata');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true,
            ]);
            $table->addColumn('path', 'string', ['notnull' => true, 'length' => 2000]);
            $table->addColumn('authors', 'text', ['notnull' => false]);
            $table->addColumn('people', 'text', ['notnull' => false]);
            $table->addColumn('locations', 'text', ['notnull' => false]);
            $table->addColumn('events', 'string', ['notnull' => false, 'length' => 2000]);
            $table->addColumn('years', 'string', ['notnull' => false, 'length' => 2000]);
            $table->addColumn('months', 'string', ['notnull' => false, 'length' => 2000]);
            $table->addColumn('topics', 'text', ['notnull' => false]);
            $table->addColumn('source_url', 'string', ['notnull' => false, 'length' => 2000]);
            $table->addColumn('image_hash', 'string', ['notnull' => false, 'length' => 2000]);
            $table->addColumn('original_publisher', 'string', ['notnull' => false, 'length' => 200]);
            $table->addColumn('copyright_holder', 'string', ['notnull' => false, 'length' => 200]);
            $table->addColumn('number_of_copy', 'integer', ['notnull' => false]);
            $table->addColumn('state', 'string', ['notnull' => false, 'length' => 20]);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['path'],'dlmt_path_index');
        }
        return $schema;
    }
}


?>