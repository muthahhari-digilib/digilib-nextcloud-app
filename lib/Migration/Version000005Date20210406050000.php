<?php

namespace OCA\DigitalLibraryApp\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;


class Version000005Date20210406050000 extends SimpleMigrationStep {
    /**
    * @param IOutput $output
    * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
    * @param array $options
    * @return null|ISchemaWrapper
    */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();

        $table = $schema->getTable('digital_library_metadata');
        $table->addColumn('create_user', 'string', ['notnull' => true, 'length' => 64]);
        $table->addColumn('create_date', 'datetime');

        $table->addColumn('write_user', 'string', ['notnull' => false, 'length' => 64]);
        $table->addColumn('write_date', 'datetime', ['notnull' => false]);

        $table->addColumn('publish_user', 'string', ['notnull' => false, 'length' => 64]);
        $table->addColumn('publish_date', 'datetime', ['notnull' => false]);

        return $schema;
    }
}


?>