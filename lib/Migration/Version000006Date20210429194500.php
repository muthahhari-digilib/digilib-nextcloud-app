<?php

namespace OCA\DigitalLibraryApp\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;


class Version000006Date20210429194500 extends SimpleMigrationStep {
    /**
    * @param IOutput $output
    * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
    * @param array $options
    * @return null|ISchemaWrapper
    */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();

        $table = $schema->getTable('digital_library_metadata');
        if (!$table->hasColumn('topic')) {
            $table->addColumn('topic', 'string', ['notnull' => false, 'length' => 64]);
        }
        if (!$table->hasColumn('type')) {
            $table->addColumn('type', 'string', ['notnull' => false, 'length' => 64]);
        }
        
        return $schema;
    }
}


?>