<?php
/*
 * Copyright (c) 2020. The Nextcloud Bookmarks contributors.
 *
 * This file is licensed under the Affero General Public License version 3 or later. See the COPYING file.
 */

namespace OCA\Bookmarks\Settings;

use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\Settings\IIconSection;

class AdminSection implements IIconSection {

	/** @var IL10N */
	private $l;

	/** @var IURLGenerator */
	private $urlgen;

	public function __construct(IL10N $l, IURLGenerator $urlgen) {
		$this->l = $l;
		$this->urlgen = $urlgen;
	}

	/**
	 * returns the ID of the section. It is supposed to be a lower case string
	 *
	 *
	 * @return string
	 */
	public function getID() {
		return 'digitallibraryapp';
	}

	/**
	 * returns the translated name as it should be displayed, e.g. 'LDAP / AD
	 * integration'. Use the L10N service to translate it.
	 *
	 * @return string
	 */
	public function getName() {
		return $this->l->t('#Library Tags');
	}

	/**
	 * @return string
	 */
	public function getIcon() {
		return 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMTYiIHdpZHRoPSIxNiIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgMTYgMTYiPjxnIHN0cm9rZT0iIzAwMCIgZmlsbD0ibm9uZSI+PGNpcmNsZSBzdHJva2Utd2lkdGg9IjIiIGN5PSIxMCIgY3g9IjUiIHI9IjMiIGZpbGw9IiMwMDAiLz48cGF0aCBkPSJtNy43IDcuOCA1LjgtNS44IiBzdHJva2Utd2lkdGg9IjIiIGZpbGw9IiMwMDAiLz48cGF0aCBkPSJtMTEuNSAzLjUgMi41IDIuNSIgc3Ryb2tlLXdpZHRoPSIxLjUiIGZpbGw9IiMwMDAiLz48cGF0aCBkPSJtOS4zIDUgMi41IDIuNSIgc3Ryb2tlLXdpZHRoPSIxLjUiIGZpbGw9IiMwMDAiLz48L2c+PC9zdmc+Cg==';
	}

	/**
	 * @return int whether the form should be rather on the top or bottom of the settings navigation. The sections are arranged in ascending order of the priority values. It is required to return a value between 0 and 99.
	 */
	public function getPriority() {
		return 100;
	}
}
