<?php
/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\DigitalLibraryApp\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
    'routes' => [
		['name' => 'Metadata#index', 'url' => '/metadata/{docId}', 'verb' => 'GET'],
		['name' => 'Metadata#update', 'url' => '/metadata', 'verb' => 'POST'],
		['name' => 'Metadata#options', 'url' => '/options', 'verb' => 'GET'],
		['name' => 'Metadata#artwork', 'url' => '/artwork/{artworkFile}', 'verb' => 'GET'],
		['name' => 'Metadata#uploadArtwork', 'url' => '/artwork', 'verb' => 'POST'],
		['name' => 'Metadata#count', 'url' => '/getMetadatas', 'verb' => 'POST'],
		['name' => 'Metadata#addTag', 'url' => '/tag', 'verb' => 'PUT'],

		['name' => 'AdminSettings#index', 'url' => '/digitallibraryapp', 'verb' => 'GET'],

		['name' => 'Admin#isDigilibAdmin', 'url' => '/admin', 'verb' => 'GET']
	]
];
