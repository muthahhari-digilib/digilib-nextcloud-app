-- tempatkan di server, di homedir nya digilibSync
create temporary table __sync__ as select * from library_content where false;

\copy __sync__ from '/tmp/__sync' with (format csv, header);

UPDATE library_content AS l
   SET "content_type" = case when s."content_type" is null then l."content_type" else s."content_type" end,
       file_name = s.file_name,
       "description" = s."description",
       "path" = s."path",
       author = s.author,
       people_name = s.people_name,
       event_name = s.event_name,
       "month" = s."month",
       "year" = s."year",
       featured_image = s.featured_image,
       original_publisher = s.original_publisher,
       source_url = s.source_url,
       is_highlight = s.is_highlight,
       is_published = s.is_published,
       copyright = s.copyright,
       number_of_stocks = s.number_of_stocks,
       content_length = case when s.content_length is null then l.content_length else s.content_length end,
       created_by = s.created_by,
       created_at = s.created_at,
       modified_by = s.modified_by,
       modified_at = s.modified_at,
       "type" = s."type",
       topic = s.topic,
       "location" = s."location",
       topics = s.topics
  FROM __sync__ s
 WHERE s.id = l.id;

INSERT INTO library_content
    (id, "content_type", "description", "path", author, people_name, event_name, "month", "year", featured_image,
     original_publisher, source_url, is_highlight, is_published, copyright, number_of_borrowed,
     number_of_stocks, content_length, created_by, created_at, modified_by, modified_at, "type", topic,
     "location", topics)
SELECT id, "content_type", "description", "path", author, people_name, event_name, "month", "year",
       featured_image, original_publisher, source_url, is_highlight, is_published, copyright, 0, number_of_stocks,
       content_length, created_by, created_at, modified_by, modified_at, "type", topic, "location", topics
  FROM __sync__ s
 WHERE s.id not in (select id from library_content);
 