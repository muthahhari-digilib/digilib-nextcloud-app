import pg8000.native
import time
import os
from os import environ
import os.path
import subprocess
import sys
import csv
import hashlib
import glob
import shlex


CHECK_TIMEOUT = 30 # 5 menit

STATE_PENDING_UNPUBLISH = 'Pending Unpublish'
STATE_UNPUBLISHED = 'Unpublished'
STATE_PENDING_PUBLISH = 'Pending Publish'
STATE_PUBLISHING = 'Publishing'
STATE_PENDING_SYNC_METADATA = 'Pending Sync Metadata'
STATE_SYNCING_METADATA = 'Syncing Metadata'
STATE_PENDING_SYNC_CONTENT = 'Pending Sync Content'
STATE_PUBLISHED = 'Published'
STATE_ERROR_UNPUBLISHED = 'Error (Unpublished)'
STATE_DELETE = (
    STATE_UNPUBLISHED,
    STATE_PUBLISHED,
    STATE_ERROR_UNPUBLISHED,
)

NEXTCLOUD_DATADIR = environ.get('NEXTCLOUD_DATADIR', '/var/lib/nextcloud/data')
TMP_DIR = environ.get('TMP_DIR', '/tmp/syncprocess/')

username = environ.get('PGUSER', 'postgres')
password = environ.get('PGPASSWORD', 'cpsnow')
host = environ.get('PGHOST', 'localhost')
port = environ.get('PGPORT', '5432')
database = environ.get('PGDATABASE', 'nextcloud')
tableprefix = environ.get('PGPREFIX', 'oc_')

ssh_userserver = 'digilibSync@51.79.143.50'

def rsync_command(source, target):
    if not target.startswith('/'):
        target = '~/' + target
    ret = [
        'rsync',
        '-azv',
        '--protect-args',
        '--progress',
        source,
        f'{ssh_userserver}:{target}'
    ]
    print('Rsync command: ' + str(ret))
    return ret

os.makedirs(TMP_DIR, exist_ok=True)

con = pg8000.dbapi.Connection(username, password=password, host=host, port=port, database=database)
con.autocommit = True
c = con.cursor()


def rsync_to_server(source, target):
    res = subprocess.run(['chmod', '-Rf', 'u+rwX,go+rX,go-w', source])
    if res.returncode:
        print(f'gagal chmod {tmp_file_path}', file=sys.stderr)
        raise RuntimeError(f'Gagal chmod {tmp_file_path}')
    
    print(f'Rsync {source} --> {target}')
    res = subprocess.run(['ssh',ssh_userserver,'mkdir','-p', shlex.quote(os.path.split(target)[0])])
    if res.returncode:
        print(f'gagal create dir untuk {target}', file=sys.stderr)
        raise RuntimeError(f'Gagal create dir untuk {target}')
    res = subprocess.run(rsync_command(source, '/tmp/digilibSync.1'))
    if res.returncode:
        print(f'gagal sync {source} --> {target}', file=sys.stderr)
        raise RuntimeError(f'Gagal sync {source} --> server di {target}')
    res = subprocess.run(['ssh',ssh_userserver,'mv','/tmp/digilibSync.1',shlex.quote(target)])

def myjoin(*args):
    arg2 = [args[0]] + [a[1:] if a.startswith('/') else a for a in args[1:]]
    return os.path.join(*arg2)

def setState(row, state):
    print(f'Setting state of {row["path"]} to {state}')
    query = f'UPDATE {tableprefix}digital_library_metadata SET state=%s WHERE path=%s;'
    params = [state, row['path']]

    if state in STATE_DELETE:
        c.execute(f'DELETE FROM {tableprefix}digital_library_sync WHERE path=%s;',
            [row['path']])

    c.execute(query, params)
    row['state'] = state


def syncAudio(row):
    basedir, filepath = row['file_path'].split('$$$$$/////$$$$$')
    source_file_path = myjoin(NEXTCLOUD_DATADIR, basedir, filepath)

    tmp_file_path = myjoin(TMP_DIR, os.path.basename(filepath))
    
    # samakan volume
    print(f'normalizing audio {source_file_path} -> {tmp_file_path}')
    res = subprocess.run(['ffmpeg-normalize', source_file_path, '-c:a', 'mp3', '-o', tmp_file_path])
    if res.returncode:
        print(f'gagal proses {source_file_path}', file=sys.stderr)
        # jika gagal, pakai yang asli
        tmp_file_path = source_file_path
    
    # cek panjang
    res = subprocess.run(['ffmpeg', '-i', tmp_file_path, '-f', 'null', '-'], capture_output=True, encoding='utf-8')
    audiolen = -1
    if not res.returncode:
        timestr = re.search('time=[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]+',res.stderr[-200:])
        if timestr:
            audiolen = int(round(time.mktime(time.strptime(timestr.group(), 'time=%H:%M:%S:%f'))))
    
    # sync ke server
    rsync_to_server(tmp_file_path, filepath)
    
    return audiolen


def syncBook(row):
    basedir, filepath = row['file_path'].split('$$$$$/////$$$$$')
    source_file_path = myjoin(NEXTCLOUD_DATADIR, basedir, filepath)
    tmp_file_path = myjoin(TMP_DIR, os.path.basename(filepath)[:-4], '')
    target_filepath = 'books/' + hashlib.md5(row['file_path'].encode('utf-8')).hexdigest() + '/' + os.path.basename(filepath)[:-4]
    
    print('running ' + ' '.join(['pdf2svg', source_file_path, tmp_file_path + '%03d.svg', 'all']))
    os.makedirs(tmp_file_path, exist_ok=True)
    res = subprocess.run(['pdf2svg', source_file_path, tmp_file_path + '%03d.svg', 'all'])
    if res.returncode:
        print(f'gagal proses {source_file_path}', file=sys.stderr)
        raise RuntimeError(f'Gagal pdf2svg {source_file_path}')

    rsync_to_server(tmp_file_path, target_filepath)
    return (target_filepath, len(glob.glob(tmp_file_path + '*.svg')))


IMAGE_EXTENSIONS = ['jpg', 'jpeg', 'png', 'gif']
def syncArtwork(row):
    if row['image_hash'] and row['image_hash'].strip():
        file_path = '/opt/digilibaw/' + row['image_hash']
        rsync_to_server(file_path, '_artworks/' + row['image_hash'][:2] + '/' + row['image_hash'])
    else:
        basedir, filepath = row['file_path'].split('$$$$$/////$$$$$')
        source_file_path = myjoin(NEXTCLOUD_DATADIR, basedir, filepath)
        if any(source_file_path.endswith('.' + ext) for ext in IMAGE_EXTENSIONS):
            tmp_file_path = myjoin(TMP_DIR, os.path.basename(filepath))
            # resize
            res = subprocess.run(['ffmpeg','-i',source_file_path,'-vf','scale=-1:512',tmp_file_path])
            if res.returncode:
                print(f'gagal proses {source_file_path}', file=sys.stderr)
                # jika gagal, pakai yang asli
                tmp_file_path = source_file_path
            with open(tmp_file_path, "rb") as f:
                file_hash = hashlib.md5()
                while chunk := f.read(8192):
                    file_hash.update(chunk)
            
            hashfile = file_hash.hexdigest() + '.' + os.path.basename(filepath).split('.')[-1]
            rsync_to_server(file_path, hashfile[:2] + '/' + hashfile)


def syncMetadata(row, contentType=None, content_length=None, filepath = None):
    columns = 'id,content_type,file_name,description,path,author,people_name,' \
              'event_name,month,year,featured_image,original_publisher,source_url,'\
              'is_highlight,is_published,copyright,number_of_borrowed,number_of_stocks,'\
              'content_length,created_by,created_at,modified_by,modified_at,type,topic,'\
              'location,topics'
    if not filepath:
        basedir, filepath = row['file_path'].split('$$$$$/////$$$$$')
    tmp_file_path = myjoin(TMP_DIR, '__sync.csv')
    with open(tmp_file_path, 'w', newline='') as csvfile:
        wrt = csv.writer(csvfile)
        wrt.writerow(columns.split(','))
        wrt.writerow([
            row['path'].replace('/','___'),    # id pakai id file, bukan id row
            contentType,
            filepath,
            row['description'],
            row['digilib_path'],
            row['authors'],
            row['people'],
            row['events'],
            row['months'],
            row['years'],
            'https://digilib.ijabi.my.id/images/' + (row['image_hash'] and (row['image_hash'][:2] + '/' + row['image_hash']) or row['image_hash']),
            row['original_publisher'],
            row['source_url'],
            'Featured Content' in (row['topics'] or '').split(';'),
            1,
            row['copyright_holder'],
            None,
            row['number_of_copy'],
            content_length,
            row['create_user'],
            row['create_date'],
            row['write_user'],
            row['write_date'],
            row['type'],
            row['topic'],
            row['locations'],
            row['topics'],
        ])
    rsync_to_server(tmp_file_path, f'/tmp/__sync')  # file utama yang akan dipanggil __sync.sql
    logfile = f'/tmp/__sync.{time.time()}'
    rsync_to_server(tmp_file_path, logfile) # untuk checking jika perlu
    
    # eksekusi __sync.sql di server
    cmdline = ['ssh',ssh_userserver,'psql','-a','-f','__sync.sql','-L','{logfile}.log','-w','-d','digital-library']
    res = subprocess.run(cmdline)
    if res.returncode:
        print(f'gagal proses {cmdline}', file=sys.stderr)

def getVideoLen(row):
    #TODO: get from YouTube??
    basedir, filepath = row['file_path'].split('$$$$$/////$$$$$')
    source_file_path = os.path.join(NEXTCLOUD_DATADIR, basedir, filepath)
    res = subprocess.run(['ffprobe', '-i', source_file_path], capture_output=True, encoding='utf-8')
    if not res.returncode:
        timestr = re.search('Duration: [0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]+', res.stderr[-200:])
        if timestr:
            return int(round(time.mktime(time.strptime(timestr.group(), 'time=%H:%M:%S:%f'))))
    return -1

def isAudio(filepath):
    return filepath.endswith('.mp3')

def isVideo(filepath):
    return any(filepath.endswith('.' + ext) for ext in ['mp4', 'mkv', 'mov', 'mpeg', 'mpg', 'divx', 'xvid', 'ogv', 'flv'])

def isPdfBook(filepath):
    return filepath.endswith('.pdf')

def isImage(filepath):
    return any(filepath.endswith('.' + ext) for ext in IMAGE_EXTENSIONS)

def process(row):
    print(f'Processing {row["path"]}, state={row["state"]}')
    if row['state'] == STATE_PENDING_PUBLISH:
        filepath = row['file_path'].lower()
        if isAudio(filepath):
            audiolen = -1
            try:
                audiolen = syncAudio(row)
                syncArtwork(row)
                setState(row, STATE_PENDING_SYNC_METADATA)
            except RuntimeError as e:
                setState(row, STATE_ERROR_UNPUBLISHED)
                raise
            syncMetadata(row, 'audio', audiolen)
        elif isVideo(filepath):
            videolen = getVideoLen(row)
            syncArtwork(row)
            setState(row, STATE_PENDING_SYNC_METADATA)
            syncMetadata(row, 'video', videolen)
        elif isPdfBook(filepath):
            target_filepath, booklen = syncBook(row)
            syncArtwork(row)
            setState(row, STATE_PENDING_SYNC_METADATA)
            syncMetadata(row, 'book', booklen, filepath=target_filepath)
        elif isImage(filepath):
            syncArtwork(row)
            setState(row, STATE_PENDING_SYNC_METADATA)
            syncMetadata(row, 'photo', videolen)
        else:
            raise RuntimeError(f'unsupported file type: {filepath}')
        setState(row, STATE_PUBLISHED)
    elif row['state'] == STATE_PENDING_SYNC_METADATA:
        syncMetadata(row)
        setState(row, STATE_PUBLISHED)

print (f'Nextcloud dir: {NEXTCLOUD_DATADIR}')
while True:
    last_check = time.time()

    c.execute(f'''
        SELECT *
          FROM {tableprefix}digital_library_sync s
          JOIN {tableprefix}digital_library_metadata m
         USING (path);
    ''')
    rows = c.fetchall()
    if rows:
        print(f'Ada {len(rows)} data yang perlu disync')
        keys = [k[0] for k in c.description]
        results = [dict(zip(keys, row)) for row in rows]

        for res in results:
            process(res)

    if delta := (last_check + CHECK_TIMEOUT - time.time()) > 0:
        time.sleep(delta)
