import Vue from 'vue'
import { translate as t } from '@nextcloud/l10n'
import { generateUrl } from '@nextcloud/router'
import axios from '@nextcloud/axios'
import DigilibTab from './views/DigilibTab'

// Init Sharing tab component
const View = Vue.extend(DigilibTab)
let tabInstance = null

window.addEventListener('DOMContentLoaded', function() {
	if (OCA.Files && OCA.Files.Sidebar) {
		const digilibTab = new OCA.Files.Sidebar.Tab({
			id: 'digitallibraryapp',
			name: t('digitallibraryapp', '#Library'),
			icon: 'icon-category-auth',
			mount(el, fileInfo, context) {
				if (tabInstance) {
					tabInstance.$destroy()
				}
				tabInstance = new View({
					// Better integration with vue parent component
					parent: context,
				})
				// Only mount after we have all the info we need
				tabInstance.update(fileInfo)
				tabInstance.$mount(el)
			},
			update(fileInfo) {
				tabInstance.update(fileInfo)
			},
			destroy() {
				tabInstance.$destroy()
				tabInstance = null
			},
			enabled(fileInfo) {
				return (fileInfo.type === 'file')
			},
		})
		OCA.Files.Sidebar.registerTab(digilibTab)
	} else {
		alert('test')
	}
});

(function() {
	if (!OCA.Digilib) {
		OCA.Digilib = {}

		/**
		 * @namespace
		 */
		OCA.Digilib.Util = {
			/**
			 * Initialize the sharing plugin.
			 *
			 * Registers the "Share" file action and adds additional
			 * DOM attributes for the sharing file info.
			 *
			 * @param {OCA.Files.FileList} fileList file list to be extended
			 */
			attach(fileList) {
				const oldCreateRow = fileList._createRow
				fileList._createRow = function(fileData) {
					const tr = oldCreateRow.apply(this, arguments)
					if (fileData.metadataCount) {
						tr[0].querySelector('.nametext')
						  .insertAdjacentHTML(
							  'beforeend',
							  '<span class="digilib-metadata-count">' + fileData.metadataCount + '</span>')
					}
					return tr
				}

				const oldReload = fileList.reload
				const oldReloadCallback = fileList.reloadCallback
				const deferred = window.jQuery.Deferred().promise()
				fileList.reload = function() {
					oldReload.apply(this)

					return deferred.promise()
				}

				fileList.reloadCallback = async function(success, result) {
					const dataToSearch = result.map(el => {
						return {
							id: el.id,
							path: el.path,
							name: el.name,
						}
					})
					const url = generateUrl('/apps/digitallibraryapp/getMetadatas')
					const metadatas = await axios.post(url, {
						data: dataToSearch,
					})
					result.forEach(el => {
						const path = el.path.endsWith('/') ? el.path : (el.path + '/')
						el.metadataCount = metadatas.data[path + el.name]
					})
					const res = oldReloadCallback.apply(this, [success, result])
					deferred.resolve(res)
				}
			},
		}
	}
})()

OC.Plugins.register('OCA.Files.FileList', OCA.Digilib.Util)
