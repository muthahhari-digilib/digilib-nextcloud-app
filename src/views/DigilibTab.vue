<template>
	<div
		class="digilib-metadata"
		@keydown.enter.left.right.stop>
		<actions>
			<ActionButton
				v-if="!editing && !addingTag && !publishing"
				icon="icon-edit"
				:disabled="loading"
				@click="edit">
				Edit
			</ActionButton>

			<ActionButton
				v-if="editing && !addingTag && !publishing"
				class="digilib-save-button"
				icon="icon-checkmark"
				:disabled="loading"
				@click="save">
				Save
			</ActionButton>

			<ActionButton
				v-if="userIsAdmin && !addingTag && !publishing"
				icon="icon-add"
				:disabled="loading"
				@click="addingTag = true">
				Add New Tag
			</ActionButton>

			<ActionButton
				v-if="addingTag"
				class="digilib-save-button"
				icon="icon-checkmark"
				:disabled="loading"
				@click="addTag">
				Save New Tag
			</ActionButton>

			<ActionButton
				v-if="addingTag"
				icon="icon-close"
				:disabled="loading"
				@click="addingTag = false">
				Cancel
			</ActionButton>

			<ActionButton
				v-if="userIsAdmin && !addingTag && !publishing"
				icon="icon-add"
				:disabled="loading"
				@click="editPublish">
				Publish...
			</ActionButton>

			<ActionButton
				v-if="publishing"
				class="digilib-save-button"
				icon="icon-checkmark"
				:disabled="loading"
				@click="publish">
				Save Publishing Info
			</ActionButton>

			<ActionButton
				v-if="publishing"
				icon="icon-close"
				:disabled="loading"
				@click="publishing = false">
				Cancel
			</ActionButton>
		</actions>

		<div v-if="publishing">
			<label v-if="!isImage">Artwork<br>
				<img :src="artworkUrl" class="digilib-artwork"><br>
				<input
					type="file"
					@change="filesChange($event.target.name, $event.target.files)">
			</label>
			<label>Tipe Konten<br>
				<select v-model="value['type']">
					<option
						v-for="type in types"
						:key="type"
						:value="type">
						{{ type }}
					</option>
				</select>
			</label>
			<label>Description<br>
				<textarea v-model="value['description']" />
			</label>
			<label>Source URL (diisi untuk IG, YT, FB)<br>
				<input v-model="value['sourceUrl']" type="text" style="width:100%">
			</label>
			<label>Publish (Current status: {{ value['state'] }})<br>
				<select v-model="value['publishState']">
					<option>Publish</option>
					<option>Unpublish</option>
				</select>
			</label>
			<p>Publish info</p>
			<table>
				<tr><th>Publish path</th><td>{{ publishPath }}</td></tr>
				<tr><th>File content type</th><td>{{ fileInfo.mimetype }}</td></tr>
				<tr><th>Digilib content type</th><td>{{ contentType }}</td></tr>
			</table>
		</div>

		<div v-if="!publishing">
			<label v-if="addingTag">Tag Baru
				<select v-model="newTagType">
					<option
						v-for="type in tagTypes"
						:key="type.label"
						:value="type.value">
						{{ type.label }}
					</option>
				</select>

				<input
					v-model="newTagTag"
					class="digilib-new-tag"
					type="text"
					placeholder="Tag baru">
			</label>

			<label
				v-if="!addingTag && userIsAdmin"
				style="display:inline-block">
				Jenis
				<select
					v-model="value['type']"
					:disabled="!editing">
					<option
						v-for="type in types"
						:key="type"
						:value="type">
						{{ type }}
					</option>
				</select>
			</label>

			<label
				v-if="!addingTag"
				style="display:inline-block">
				Topik
				<select
					v-model="value['topic']"
					:disabled="!editing">
					<option
						v-for="topic in topics"
						:key="topic">
						{{ topic }}
					</option>
				</select>
			</label>

			<label v-if="!addingTag">Author
				<Multiselect
					v-model="value['authors']"
					tag-placeholder="Add this author"
					placeholder="Search author"
					:loading="loading"
					:options="options['authors']"
					:multiple="true"
					:close-on-select="true"
					:clear-on-select="true"
					:disabled="!editing" />
			</label>

			<label v-if="!addingTag">Tags
				<ul class="digilib-tags">
					<li
						v-for="tag in value['tags']"
						:key="tag.label">
						{{ tag.label }}
						<div
							v-if="editing"
							class="icon icon-delete"
							@click="remove(tag)">
							&nbsp;
						</div>
					</li>
				</ul>

				<Multiselect
					v-if="editing"
					id="digilib-tags"
					ref="digilibtags"
					v-model="value['tags']"
					tag-placeholder="Tambahkan tag ini"
					placeholder="Cari tag"
					label="label"
					track-by="label"
					open-direction="bottom"
					class="hidetags"
					:loading="loading"
					:options="options['tags']"
					:block-keys="['Backspace', 'Delete']"
					:multiple="true"
					:close-on-select="true"
					:hide-selected="true"
					:disabled="!editing" />
			</label>
		</div>
	</div>
</template>

<script>
import { generateUrl } from '@nextcloud/router'
import axios from '@nextcloud/axios'
import Multiselect from '@nextcloud/vue/dist/Components/Multiselect'
import ActionButton from '@nextcloud/vue/dist/Components/ActionButton'
import { getCurrentUser } from '@nextcloud/auth'

export default {
	name: 'DigilibTab',

	components: {
		ActionButton,
		Multiselect,
	},

	mixins: [],

	data() {
		return {
			loading: true,
			editing: false,
			value: null,
			options: null,
			addingTag: false,
			newTagType: null,
			newTagTag: null,
			publishing: false,
			isAdmin: false,
			topics: [
				'Ahlul Bait',
				'Tafsir Al-Qur\'an',
				'Hadits',
				'Filsafat',
				'Sejarah',
				'Psikologi',
				'Sains dan Pendidikan',
				'Komunikasi',
				'Agama',
				'Fikih',
				'Sosial Politik',
				'Tasawuf',
				'Neurosains',
				'Do\'a',
			],
			types: [
				'Buku',
				'Audio',
				'Video',
				'Artikel',
				'Karya Seni',
				'Foto',
				'Instagram',
				'Lainnya',
			],
		}
	},

	computed: {
		/**
		 * Returns the current active tab.
		 * Needed because AppSidebarTab also uses $parent.activeTab.
		 *
		 * @returns {string}
		 */
		activeTab() {
			return this.$parent.activeTab
		},

		artworkUrl() {
			return generateUrl('/apps/digitallibraryapp/artwork/' + this.value.imageHash)
		},

		publishPath() {
			const paths = this.fileInfo.path.substring(1).split('/')
			paths.shift()
			return '/' + paths.join('/')
		},

		isImage() {
			const ext = this.fileInfo.name.split('.').pop().toLowerCase()
			return [
				'jpg', 'jpeg',
				'png', 'bmp', 'gif',
			].indexOf(ext) !== -1
		},

		contentType() {
			return {
				'image/jpeg': 'photo',
				'image/png': 'photo',
				'image/gif': 'photo',
				'video/mpeg': 'video',
				'video/mp4': 'video',
				'video/mkv': 'video',
				'audio/mpeg': 'audio',
				'audio/ogg': 'audio',
				'application/pdf': 'book',
			}[this.fileInfo.mimetype] || '<unsupported>'
		},

		userIsAdmin() {
			return getCurrentUser().isAdmin || this.isAdmin
		},

		tagTypes() {
			return [
				{ value: 'authors', label: 'Author' },
				{ value: 'people', label: 'Orang' },
				{ value: 'locations', label: 'Lokasi' },
				{ value: 'events', label: 'Event' },
				{ value: 'years', label: 'Tahun' },
				{ value: 'months', label: 'Bulan' },
				{ value: 'topics', label: 'Topik' },
			]
		},
	},

	methods: {
		/**
		 * Update current fileInfo and fetch new data.
		 * @param {Object} fileInfo the current file FileInfo.
		 */
		update(fileInfo) {
			this.resetState()
			this.fileInfo = fileInfo

			this.getDataFromApi()
		},

		/**
		 * Reset the current view to its default state
		 */
		resetState() {
			this.editing = false
			this.addingTag = false
			this.publishing = false

			this.value = {
				authors: [],
				years: [],
				months: [],
				topics: [],
				tags: [],
				topic: null,
				type: null,
				sourceUrl: null,
			}
			this.options = {
				authors: ['Loading...'],
				years: ['Loading...'],
				months: ['Loading...'],
				topics: ['Loading...'],
				tags: ['Loading...'],
			}
		},

		async getDataFromApi() {
			const adminUrl = generateUrl('/apps/digitallibraryapp/admin')
			const optionsUrl = generateUrl('/apps/digitallibraryapp/options')
			const metadataUrl = generateUrl('/apps/digitallibraryapp/metadata/' + this.fileInfo.id)
			const response = await Promise.all([
				axios.get(adminUrl),
				axios.get(optionsUrl),
				axios.get(metadataUrl),
			])

			const adminInfo = response[0].data
			const options = response[1].data
			const value = response[2].data

			this.isAdmin = adminInfo.isDigilibAdmin

			const orEmpty = arr => {
				if (arr === null || arr === undefined || arr === false) {
					return []
				}
				return arr
			}

			const processTags = obj => {
				obj.tags = []
				obj.tags = orEmpty(obj.people).map(v => {
					return {
						type: 'people',
						tag: v,
						label: ('Orang: ' + v),
					}
				}).concat(
					orEmpty(obj.locations).map(v => {
						return {
							type: 'locations',
							tag: v,
							label: ('Lokasi: ' + v),
						}
					})
				).concat(
					orEmpty(obj.events).map(v => {
						return {
							type: 'events',
							tag: v,
							label: ('Event: ' + v),
						}
					})
				).concat(
					orEmpty(obj.topics).map(v => {
						return {
							type: 'topics',
							tag: v,
							label: ('Topik: ' + v),
						}
					})
				).concat(
					orEmpty(obj.years).map(v => {
						return {
							type: 'years',
							tag: v,
							label: ('Tahun: ' + v),
						}
					})
				).concat(
					orEmpty(obj.months).map(v => {
						return {
							type: 'months',
							tag: v,
							label: ('Bulan: ' + v),
						}
					})
				)
			}

			processTags(options)
			processTags(value)

			value.publishState = ['Unpublished', 'Pending Unpublish'].indexOf(value.state) === -1 ? 'Publish' : 'Unpublish'

			if (value.sourceUrl === undefined) {
				value.sourceUrl = null
			}

			this.options = options
			this.value = value
			this.loading = false

			// monkey patch comment supaya bisa pencet kanan kiri
			document.querySelector('.comment__editor div[contenteditable=true]').addEventListener('keydown', function(e) { e.stopPropagation() })
		},

		async edit() {
			this.editing = true
			document.querySelector('header.app-sidebar-header').classList.add('digilib-hide')
		},

		async editPublish() {
			this.publishing = true
			document.querySelector('header.app-sidebar-header').classList.add('digilib-hide')
		},

		async publish() {
			const url = generateUrl('/apps/digitallibraryapp/metadata')
			if (this.contentType === 'video' && (this.value.sourceUrl === null || this.value.sourceUrl.trim() === '')) {
				alert('Konten video silakan diupload manual ke YouTube, kemudian linknya dimasukkan ke Source URL')
				return
			}
			const metadata = {
				imageHash: this.value.imageHash,
				description: this.value.description,
				sourceUrl: this.value.sourceUrl,
				publishPath: this.publishPath,
				type: this.value.type,
			}
			if ((['Unpublished', 'Pending Unpublish'].indexOf(this.value.state) === -1) !== (this.value.publishState === 'Publish')) {
				if (this.value.state !== 'Published' && this.value.state !== 'Unpublished' && this.value.state !== null) {
					// jika bukan 2 status itu, tidak bisa publish / unpublish
					alert('Hanya dapat mengubah publish pada status Published atau Unpublished')
				} else if (this.value.publishState === 'Publish') {
					metadata.state = 'Pending Publish'
				} else {
					metadata.state = 'Pending Unpublish'
				}
			}

			this.loading = true
			try {
				await axios.post(url, {
					docId: this.fileInfo.id,
					metadata,
				})
			} catch (error) {
				alert('Error: ' + error)
			}
			this.loading = false
			document.querySelector('header.app-sidebar-header').classList.remove('digilib-hide')
			this.publishing = false
			this.getDataFromApi()
		},

		async filesChange(fieldName, fileList) {
			// handle file changes
			const formData = new FormData()
			const url = generateUrl('/apps/digitallibraryapp/artwork')

			if (!fileList.length) return

			// append the files to FormData
			formData.append('artworkFile', fileList[0], fileList[0].name)

			// save it
			this.loading = true
			const res = await axios.post(url, formData)
			this.loading = false

			this.value.imageHash = res.data.hash
		},

		async save() {
			const url = generateUrl('/apps/digitallibraryapp/metadata')
			const tag = v => v.tag
			const metadata = {
				type: this.value.type,
				topic: this.value.topic,
				authors: this.value.authors,
				years: this.value.tags.filter(v => v.type === 'years').map(tag),
				months: this.value.tags.filter(v => v.type === 'months').map(tag),
				topics: this.value.tags.filter(v => v.type === 'topics').map(tag),
				people: this.value.tags.filter(v => v.type === 'people').map(tag),
				locations: this.value.tags.filter(v => v.type === 'locations').map(tag),
				events: this.value.tags.filter(v => v.type === 'events').map(tag),
			}

			this.loading = true
			try {
				await axios.post(url, {
					docId: this.fileInfo.id,
					metadata,
				})
			} catch (error) {
				alert('Error: ' + error)
			}
			this.loading = false
			document.querySelector('header.app-sidebar-header').classList.remove('digilib-hide')
			this.editing = false
		},

		removeAuthor(author) {
			this.value.authors = this.value.authors.filter(v => v !== author)
		},

		remove(tag) {
			this.value.tags = this.value.tags.filter(v => v !== tag)
		},

		async addTag() {
			if (this.newTagType == null) {
				alert('Tipe tag baru harus diisi')
				return
			}
			if (this.newTagTag == null) {
				alert('Tag baru harus diisi')
				return
			}
			if (this.newTagTag.search(';') !== -1) {
				alert('Tag tidak dapat mengandung ; (titik koma)')
				return
			}
			const url = generateUrl('/apps/digitallibraryapp/tag')
			this.loading = true
			try {
				await axios.put(url, {
					type: this.newTagType,
					tag: this.newTagTag,
				})
			} catch (error) {
				alert('Error' + error)
				return
			} finally {
				this.loading = false
			}

			const labels = {}
			this.tagTypes.forEach(t => {
				labels[t.value] = t.label
			})
			this.options.tags.push({
				type: this.newTagType,
				tag: this.newTagTag,
				label: (labels[this.newTagType] + ': ' + this.newTagTag),
			})

			this.addingTag = false
			this.newTagType = null
			this.newTagTag = null
		},
	},
}
</script>

<style lang="scss">
	.digilib-metadata label{
		div.multiselect, select {
			display: block;
		}
		display: block;
	}

	.digilib-metadata actions li {
		display: inline-block;
	}

	.digilib-metadata button.action-button {
		border: 1px solid var(--color-primary);
		border-radius: 5px;
	}

	.digilib-metadata actions {
		display: block;
	}

	.digilib-metadata .digilib-tags {
		border: 1px solid rgb(219, 219, 219);
		padding: 5px;
		min-height: 40px;
	}

	.digilib-metadata .digilib-tags li {
		border: 1px solid rgb(219, 219, 219);
		display: inline-block;
		padding: 2px;
	}

	.digilib-metadata .digilib-tags li div.icon{
		display: inline-block;
	}

	.digilib-metadata .hidetags{
		.multiselect__tag {
			display: none !important;
		}
		.multiselect__tags-wrap:before {
			content: '+ Tambah tags';
			color: darkgray;
		}
	}

	.digilib-metadata .digilib-artwork {
		height: 128px;
	}

	.digilib-metadata textarea {
		width: 100%;
	}

	.digilib-hide {
		display: none !important;
	}

	.digilib-save-button {
		background-color: var(--color-primary);
		border-radius: 5px;
	}

	.digilib-save-button * {
		color: var(--color-primary-text);
	}

	.digilib-metadata-count::after {
		content: ')';
	}

	.digilib-metadata-count::before {
		content: '(progress: ';
	}

	@media (max-width: 400px) {
		.digilib-metadata-count::before {
			content: 'p:';
		}

		.digilib-metadata-count::after {
			content: '';
		}

		.digilib-metadata-count {
			font-size: 0.7em;
		}
	}

	.digilib-metadata-count {
		color: var(--color-text-lighter);
		margin-left: 5px;
	}

	.digilib-new-tag {
		width: 100%;
	}
</style>
